import React, { Component } from 'react';
import "./home.css";
import Header from "./header";
import Slider from "./slider";
import Smartphone from "./smartphone";
import Laptop from "./laptop";
import Promotion from "./promotion";


class Home extends Component {
    render() {
        return (
            <div>
                <Header />
                <Slider/>
                <Smartphone/>
                <Laptop/>
                <Promotion/>

            </div>
        );
    }
}

export default Home;