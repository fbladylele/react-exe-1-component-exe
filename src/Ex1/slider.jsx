import React, { Component } from "react";
import "./slider.css";
import slide1 from "../assets/img/slide_1.jpg";
import slide2 from "../assets/img/slide_2.jpg";
import slide3 from "../assets/img/slide_3.jpg";
class Slider extends Component {
  render() {
    return (
      <div className="slider">
        <div id="demo" class="carousel slide" data-ride="carousel">
          <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
          </ul>

          <div class="carousel-inner">
            <div class="carousel-item active">
              <img
                src={slide1}
                alt="Los Angeles"
                width="100%"
                height="600"
              />
            </div>
            <div class="carousel-item">
              <img
                src={slide2}
                alt="Chicago"
                width="100%"
                height="600"
              />
            </div>
            <div class="carousel-item">
              <img
                src={slide3}
                alt="New York"
                width="100%"
                height="600"
              />
            </div>
          </div>

          <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
          </a>
          <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
          </a>
        </div>
      </div>
    );
  }
}

export default Slider;
