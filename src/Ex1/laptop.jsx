import React, { Component } from "react";
import "./laptop.css";
import hp from "../assets/img/slide_1.jpg";
import lenovo from "../assets/img/slide_1.jpg";
import macbook from "../assets/img/slide_1.jpg";
import rog from "../assets/img/slide_1.jpg";
// ko sd hinh ảnh đuôi pgn dc?

class Laptop extends Component {
  render() {
    return (
      <section
        id="laptop"
        className="container-fluid pt-5 pb-5 bg-light text-dark"
      >
        <h1 className="text-center">BEST LAPTOP</h1>
        <div className="row">
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-lg-3">
            <div className="container d-flex justify-content-around">
              <div className="card bg-light" style={{ width: 300 }}>
                <img
                  className="card-img-top"
                  src={macbook}
                  alt="Card image"
                  style={{ maxWidth: "100%", height: 250 }}
                />
                <div className="card-body text-center">
                  <h4 className="card-title text-center">MACBOOK</h4>
                  <p className="card-text">
                    The MacBook is a brand of notebook computers manufactured by
                    Apple Inc
                  </p>
                  <a href="#" className="mx-1 btn btn-primary">
                    Detail
                  </a>
                  <a href="#" className="mx-1 btn btn-danger">
                    Cart
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-lg-3">
            <div className="container d-flex justify-content-around">
              <div className="card bg-light" style={{ width: 300 }}>
                <img
                  className="card-img-top"
                  src={rog}
                  alt="Card image"
                  style={{ maxWidth: "100%", height: 250 }}
                />
                <div className="card-body text-center">
                  <h4 className="card-title text-center">ASUS ROG</h4>
                  <p className="card-text">
                    the Asus ROG Strix Flare is the latest addition to Asus'
                    lineup of ROG branded devices
                  </p>
                  <a href="#" className="mx-1 btn btn-primary">
                    Detail
                  </a>
                  <a href="#" className="mx-1 btn btn-danger">
                    Cart
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-lg-3">
            <div className="container d-flex justify-content-around">
              <div className="card bg-light" style={{ width: 300 }}>
                <img
                  className="card-img-top"
                  src={hp}
                  alt="Card image"
                  style={{ maxWidth: "100%", height: 250 }}
                />
                <div className="card-body text-center">
                  <h4 className="card-title text-center">HP OMEN</h4>
                  <p className="card-text">
                    A young global smartphone brand focusing on introducing
                    perfect sound quality
                  </p>
                  <a href="#" className="mx-1 btn btn-primary">
                    Detail
                  </a>
                  <a href="#" className="mx-1 btn btn-danger">
                    Cart
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xs-12 col-sm-6 col-md-6 col-lg-3 col-lg-3">
            <div className="container d-flex justify-content-around">
              <div className="card bg-light" style={{ width: 300 }}>
                <img
                  className="card-img-top"
                  src={lenovo}
                  alt="Card image"
                  style={{ maxWidth: "100%", height: 250 }}
                />
                <div className="card-body text-center">
                  <h4 className="card-title text-center">LENOVO THINKPAD</h4>
                  <p className="card-text">
                    The ThinkPad X1 Carbon is a high-end notebook computer
                    released by Lenovo in 2012
                  </p>
                  <a href="#" className="mx-1 btn btn-primary">
                    Detail
                  </a>
                  <a href="#" className="mx-1 btn btn-danger">
                    Cart
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Laptop;
