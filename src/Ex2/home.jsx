import React, { Component } from 'react';
import Header from "./header";
import Carousel from "./carousel";
import Footer from "./footer";
import Info from "./info";

class Home extends Component {
    render() {
        return (
            <div>
                <Header/>
                <Carousel/>
                <Info/>
                <Footer/>

            </div>
        );
    }
}

export default Home;