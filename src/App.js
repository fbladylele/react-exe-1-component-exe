import "./App.css";

//EX1 :
import Home from "./Ex1/home";

//----------------
//EX2:
// import Home from "./Ex2/home";

function App() {
  return (
    <div className="App">
      <Home />
    </div>
  );
}

export default App;
